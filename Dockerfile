FROM alpine:latest

MAINTAINER limbotech <ply.srh+limbotech@gmail.com>

##
## Handle packages
##
RUN apk update && \
	apk add --no-cache git vim curl openssh && \
	apk add --update tzdata && \
	rm -rf /var/cache/apk/*

##
## Setting timezone
##
ENV TZ=America/Santiago

##
## Alpine settings
##
ADD profile/profile /etc/profile
ADD profile/profile.d/* /etc/profile.d/
ADD git/.gitconfig /root/.gitconfig
ADD vim/.vimrc /root/.vimrc
ADD vim/.vim /root/.vim
